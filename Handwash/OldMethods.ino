/*
int xminRead; //http://bildr.org/2011/04/sensing-orientation-with-the-adxl335-arduino/
int xmaxRead;
int yminRead;
int ymaxRead;
int zminRead;
int zmaxRead;

void UpdateMinMaxRead(int x, int y, int z)
{
    xminRead = fmin(xminRead, x); 
    yminRead = fmin(yminRead, y); 
    zminRead = fmin(zminRead, z);
    xmaxRead = fmax(xmaxRead, x); 
    ymaxRead = fmax(ymaxRead, y); 
    zmaxRead = fmax(zmaxRead, z);
    /*
    char writeBuf[16];
    sprintf(writeBuf, "minRead = %d", minRead);  
    SimbleeForMobile.updateText(text4, writeBuf); 
    sprintf(writeBuf, "maxRead = %d", maxRead);  
    SimbleeForMobile.updateText(text5, writeBuf); *//*
}

void TemperatureFlipFlop()
{
  float temp = Simblee_temperature(FAHRENHEIT);
  
    // requires newlib printf float support
    char buf[16];
    sprintf(buf, "%.02f", temp);  
    if(flip)
    {
      SimbleeForMobile.updateText(text2, buf);
      sprintf(buf, "Flip");
      SimbleeForMobile.updateText(text, buf);
    }
    else
    {
      SimbleeForMobile.updateText(text3, buf);     
      sprintf(buf, "Flop");
      SimbleeForMobile.updateText(text, buf);
    }    
    flip = !flip;
}

void VibrateVariably()
{
  int temp = 82;
  float vibeIntensity = temp-82;
  vibeIntensity = vibeIntensity * 32;
  vibeIntensity = fmin(255, vibeIntensity);
  vibeIntensity = fmax(0, vibeIntensity);  
  analogWrite(VIBE_PIN, vibeIntensity);
}

void DisplayAngles(int xRead, int yRead, int zRead)
{
  UpdateMinMaxRead(xRead, yRead, zRead);
  //convert read Readues to degrees -90 to 90 - Needed for atan2
  int xAng = map(xRead, xminRead, xmaxRead, -90, 90);
  int yAng = map(yRead, yminRead, ymaxRead, -90, 90);
  int zAng = map(zRead, zminRead, zmaxRead, -90, 90);

  //Caculate 360deg values like so: atan2(-yAng, -zAng)
  //atan2 outputs the value of -π to π (radians)
  //We are then converting the radians to degrees
  int x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  int y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  int z = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  char writeBuf[16];
  sprintf(writeBuf, "XAng = %d", x);  
  SimbleeForMobile.updateText(text4, writeBuf);
  
  sprintf(writeBuf, "YAng = %d", y);  
  SimbleeForMobile.updateText(text5, writeBuf);
  
  sprintf(writeBuf, "ZAng = %d", z);  
  SimbleeForMobile.updateText(text6, writeBuf);
}
*/
