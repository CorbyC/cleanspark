#include <SimbleeForMobile.h>
#include <math.h>

//variables you may want to change
const int minHwDur = 5000; //minimum time of consecutive movement to be considered a handwash, in milliseconds
const int hwSensitivity = 40; //the readings with gentle movement seem to fluctuate between 400 and 630 by my testing. So if it changes by this value, maybe they're moving
const int zoneChgTime = 2000; //must consistently read in different zone for 2 seconds before state changes
const int HwBuffer = 15000; //amount of time before or after a needHW you should have washed your hands
const int WarningBuffer = 11000; //amount of time after a warning you have to wash your hands still
const int zoneDist = -56; //distance to zone, recommended -65


//variables you shouldn't touch--------------------------
float avgRssi = 1.0;  //unset value, since real values are <= 0
float avgTemp = -100.0;  //unset value, since real values are <= 0
const int samples = 20;
const int VIBE_PIN = 9;
const int X_PIN = 6;
const int Y_PIN = 5;
const int Z_PIN = 3;
unsigned long lastHw = 0; //the last time a handwash was completed
unsigned long hwOutset = 0; //time the last possible handwash start was detected
int oldX = 500; //used to compare current reading to previous one to see if hands are moving in a handwash motion
int oldX2 = 500; //save the previous two readings because hands might not move much in a half second interval
int oldY = 500; 
int oldY2 = 500; 
int oldZ = 500; 
int oldZ2 = 500;
int zoneState = 0; //0 in zone, 1 out of zone
int changeZone = 0;

int needHw = 0; //used to record the timestamp when a HW is needed
bool hasBeenWarned = true;
int hwSuccess = 0;
int hwWarnSuccess = 0;
int hwFail = 0;

char writeBuf[64]; 
uint8_t text;
uint8_t text2;
uint8_t text3;
uint8_t text4;
uint8_t text5;
uint8_t text6;
uint8_t text7;
uint8_t text8;
uint8_t text9;
uint8_t text10;
uint8_t text11;
uint8_t text12;

uint8_t hwLight;
color_t hwLightColor;
int hwLightStayDuration = 0;

bool flip;
unsigned long lastUpdated = 0;
unsigned long updateRate = 500; // half a second

// include newlib printf float support (%f used in sprintf below)
asm(".global _printf_float");

void setup()
{
  // this is the data we want to appear in the advertisement
  // (if the deviceName and advertisementData are too long to fix into the 31 byte
  // ble advertisement packet, then the advertisementData is truncated first down to
  // a single byte, then it will truncate the deviceName)
  SimbleeForMobile.advertisementData = "CleanSpark";

  // use a subdomain to create an application specific cache
  SimbleeForMobile.domain = "cleanspark.main.com";

  // establish a baseline to use the cache during development to bypass uploading
  // the image each time
  SimbleeForMobile.baseline = "Mar 25 2017";

  // start SimbleeForMobile
  SimbleeForMobile.begin();
  
  flip = true;
  pinMode(VIBE_PIN, OUTPUT);
}


void ui()
{
  SimbleeForMobile.beginScreen(WHITE);
  // txPowerLevel can be any multiple of 4 between -20 and +4, inclusive. The
  // default value is +4; at -20 range is only a few feet.
  //SimbleeForMobile.txPowerLevel = -4; //https://learn.sparkfun.com/tutorials/simblee-concepts/control-an-led
  
  text = SimbleeForMobile.drawText(1, 1, "", BLUE);
  text2 = SimbleeForMobile.drawText(1, 31, "", BLUE);
  text3 = SimbleeForMobile.drawText(1, 61, "", BLUE);
  text4 = SimbleeForMobile.drawText(1, 91, "", BLUE);
  text5 = SimbleeForMobile.drawText(1, 121, "", BLUE);
  text6 = SimbleeForMobile.drawText(1, 151, "", BLUE);
  text7 = SimbleeForMobile.drawText(1, 241, "", BLUE);
  text8 = SimbleeForMobile.drawText(1, 271, "", BLUE);
  text9 = SimbleeForMobile.drawText(1, 301, "", BLUE);
  text10 = SimbleeForMobile.drawText(1, 361, "", BLUE);
  text11 = SimbleeForMobile.drawText(1, 391, "", BLUE);
  text12 = SimbleeForMobile.drawText(1, 421, "", BLUE);

  hwLight = SimbleeForMobile.drawRect(50, 151, SimbleeForMobile.screenWidth - 100, 90, WHITE);
  SimbleeForMobile.endScreen();
}

void SimbleeForMobile_onConnect()
{
  flip = true;
  lastHw = 0; 
  zoneState = 0;
  needHw = 0;
  avgRssi = 1.0;
  avgTemp = Simblee_temperature(FAHRENHEIT);
}

void SimbleeForMobile_onDisconnect()
{
  analogWrite(VIBE_PIN, 0);
}

void SimbleeForMobile_onRSSI(int rssi)
{
  // initialize average with instaneous value to counteract the initial smooth rise
  if (avgRssi == 1.0)
    avgRssi = rssi;

  // single pole low pass recursive IIR filter
  avgRssi = rssi * (1.0 / samples) + avgRssi * (samples - 1.0) / samples;

  if(LeavingZone()) // if leaving zone
  {
    if(changeZone == 0)
    {
      changeZone = millis();    
    }
    else
    {
      if(millis() - changeZone > zoneChgTime)
      {
        ChangeZoneState();
      }
    }
  }
  else
  {
    changeZone = 0;    
  }
}

bool LeavingZone()
{
  return ((zoneState == 0 && avgRssi < zoneDist)  //in zone and leaving
       || (zoneState == 1 && avgRssi >= zoneDist)); //our of zone and entering 
}

void ChangeZoneState()
{
  if(zoneState == 0)
  {
    zoneState = 1;
  }
  else
  {
    zoneState = 0;
  }

  TriggerHw();
}
  
void TriggerHw()
{
    if(needHw == 0)
  {
    needHw = millis();  
    hasBeenWarned = false;
  }
}

void ui_event(event_t &event)
{
}

void PrintReadings(int x, int y, int z)
{
  sprintf(writeBuf, "X = %d, Xp = %d, Xp2 = %d", x, oldX, oldX2);  
  SimbleeForMobile.updateText(text, writeBuf);
  sprintf(writeBuf, "Y = %d, Yp = %d, Yp2 = %d", y, oldY, oldY2);  
  SimbleeForMobile.updateText(text2, writeBuf);
  sprintf(writeBuf, "Z = %d, Zp = %d, Zp2 = %d", z, oldZ, oldZ2);  
  SimbleeForMobile.updateText(text3, writeBuf);      
}

void UpdateOldReads(int x, int y, int z)
{
  if(flip == true)
  {
    oldX = x;
    oldY = y;
    oldZ = z;
  }
  else
  {
    oldX2 = x;
    oldY2 = y;
    oldZ2 = z;
  }
  flip = !flip;
}

void CheckForHandwashing(int x, int y, int z)
{
  if(abs(x - oldX)  > hwSensitivity || //movement detected
     abs(x - oldX2) > hwSensitivity ||
     abs(y - oldY)  > hwSensitivity ||
     abs(y - oldY2) > hwSensitivity ||
     abs(z - oldZ)  > hwSensitivity ||
     abs(z - oldZ2) > hwSensitivity)
  {
    if(hwOutset == 0) //begin tracking handwash time   
    {
      hwOutset = millis();
    }
    else if(millis() - hwOutset > minHwDur)
    {
      lastHw = millis(); //completed a handwash!
      hwOutset = 0;
    }
  }
  else if (hwOutset > 0) //only hit if no movement is detected
  {
    hwOutset = 0;
  }  

  if(hwOutset > 0)
  {    
    sprintf(writeBuf, "HW potential current dur %d/%d", (millis() - hwOutset), minHwDur);  
    SimbleeForMobile.updateText(text4, writeBuf);
  }
  else
  {
    sprintf(writeBuf, "Not enough movement detected for HW");  
    SimbleeForMobile.updateText(text4, writeBuf);
  }

  if(lastHw > 0)
  {
    sprintf(writeBuf, "Last completed HW %d seconds ago", (int)((millis() -lastHw)/1000));  
    SimbleeForMobile.updateText(text5, writeBuf);    
  }     
  else
  {
    SimbleeForMobile.updateText(text5, "");    
  }
}

void PrintZoneState()
{
  if(avgRssi != 0)
  {
    sprintf(writeBuf, "AvgRSSI %.02f", avgRssi);  
    SimbleeForMobile.updateText(text2, writeBuf);
  }
  if(zoneState == 0)
  {  
    if(changeZone == 0)
    {
      SimbleeForMobile.updateText(text, "In zone");
    }
    else
    {
      sprintf(writeBuf, "In zone but exiting in %d/%d", (millis() - changeZone), zoneChgTime);  
      SimbleeForMobile.updateText(text, writeBuf);
    }      
  }
  else
  {     
    if(changeZone == 0)
    {
      SimbleeForMobile.updateText(text, "Out of zone");
    }
    else
    {
      sprintf(writeBuf, "Out of zone but entering in %d/%d", (millis() - changeZone), zoneChgTime);  
      SimbleeForMobile.updateText(text, writeBuf);      
    }
  }
}

void CheckNeededHandwash()
{
  if(needHw != 0)
  {
    int millRead = millis();
    //sprintf(writeBuf, "lhw %d, nhw %d, mllrd %d", lastHw, needHw, millRead);  
    //SimbleeForMobile.updateText(text7, writeBuf);

    if(lastHw != 0) 
    {      
      if( (lastHw >= (needHw - HwBuffer) && (lastHw <= (needHw + HwBuffer)))) //washed within HWBuffer milliseconds! (either before or after buffer expired)
      {
        needHw = 0;
        hwSuccess += 1;
        lastHw = 0;
        SimbleeForMobile.updateText(text8, "Hit success!");         
        SimbleeForMobile.updateColor(hwLight, BLUE);
        hwLightColor = BLUE;
        hwLightStayDuration = 8;
      }
      else if( (lastHw >= (needHw - (HwBuffer + WarningBuffer)) && (lastHw <= (needHw + (HwBuffer + WarningBuffer))))) //finished in warning buffer at least
      {
        needHw = 0;
        hwWarnSuccess += 1;
        lastHw = 0;
        SimbleeForMobile.updateText(text8, "Hit warn success!");   
        SimbleeForMobile.updateColor(hwLight, BLUE);
        hwLightColor = BLUE;
        hwLightStayDuration = 8;
      }
    }
    else if( (needHw >= (millRead - HwBuffer) && (needHw <= (millRead + HwBuffer))))
    {
      SimbleeForMobile.updateColor(hwLight, GREEN);
      //still time, do nothing
      sprintf(writeBuf, "Time left %d/%d", millRead - needHw, HwBuffer);  
      SimbleeForMobile.updateText(text8, writeBuf);
    }
    else if( (needHw >= (millRead - (HwBuffer + WarningBuffer)))) //still in buffer, have time to wash
    {
      SimbleeForMobile.updateColor(hwLight, YELLOW);
      sprintf(writeBuf, "Warning time left %d/%d", millRead - (needHw + HwBuffer), WarningBuffer);  
      SimbleeForMobile.updateText(text8, writeBuf);
      if(hasBeenWarned == false)
      {
        Warn();
        hasBeenWarned = true;
        SimbleeForMobile.updateText(text8, "Warned!");   
      }
    }
    else //outside of buffer, failed
    {
      needHw = 0;
      hwFail += 1;        
      SimbleeForMobile.updateText(text8, "Failed!");   
      SimbleeForMobile.updateColor(hwLight, RED);
      hwLightColor = RED;
      hwLightStayDuration = 8;
    }
  }
  else
  {
    if(hwLightStayDuration > 0)
    {
      hwLightStayDuration -= 1;
      SimbleeForMobile.updateColor(hwLight, hwLightColor); 
    }
    else
    {
      SimbleeForMobile.updateColor(hwLight, WHITE);
    }
  }
}

void Warn()
{
  int vibeFqcy = 500;
  for(int i = 0; i < 3; i++)
  {
    digitalWrite(VIBE_PIN, HIGH);
    delay(vibeFqcy);
    digitalWrite(VIBE_PIN, LOW);
    delay(vibeFqcy);
  }
}

void PrintNeedHWDeets()
{
  sprintf(writeBuf, "scss=%d, scssWarn=%d, fail=%d", hwSuccess, hwWarnSuccess, hwFail);  
  SimbleeForMobile.updateText(text9, writeBuf);      
}

void UpdateRollingTemp(float temp)
{
  avgTemp = temp * (1.0 / samples) + avgTemp * (samples - 1.0) / samples; 
}

void PrintTemp(float temp)
{
    sprintf(writeBuf, "tempRead=%.02f, avgTemp=%.02f", temp, avgTemp);  
    SimbleeForMobile.updateText(text10, writeBuf);
}

void loop()
{
  unsigned long loopTime = millis();
  
  // sample once per second
  // todo: Simblee_ULPDelay( SECONDS(1) );
  
  if (SimbleeForMobile.updatable && updateRate < (loopTime - lastUpdated))
  {    
    lastUpdated = loopTime;    

    int x = analogRead(X_PIN);
    int y = analogRead(Y_PIN);
    int z = analogRead(Z_PIN);

    float temp = Simblee_temperature(FAHRENHEIT);
    UpdateRollingTemp(temp);

    //PrintReadings(x, y, z);
    CheckForHandwashing(x, y, z);
    UpdateOldReads(x, y, z);  
    PrintZoneState();
    CheckNeededHandwash();
    PrintNeedHWDeets();

    PrintTemp(temp);
  }
  //analogWrite(vibe, val)  //val can be 0-255
  // process must be called in the loop for SimbleeForMobile
  SimbleeForMobile.process();
}
